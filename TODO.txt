 ### TODO ###
 ### Za većinu ovih zadataka će tijekom implementiranja biti potrebno hardkodirati neku sliku/glcm/okno, tj. potreban je dummy parametar s kojim će se testirati implementacija :) ### 
 ### Zapisati svoje ime na mjestu "NITKO" kada se preuzme zadatak tako da dvoje ljudi ne rade isti zadatak odjednom. Pushati na git. ###
 ### Staviti X ispred zadatka, ako je riješen ###
 
Kostur ------------- FD i PK
 
Preprocessing:
	Implementirati metodu resize ------------- NITKO
	Implementirati metodu to_byte_array ------------- NITKO
	Implementirati metodu grayscale ------------- NITKO
	
Feature extraction:
	X  Implementirati metodu calculate_haralick_features ------------- Tesa
	X  Implementirati metodu calculate_glc_matrix ------------- Tesa
	
Testirati (opcionalno, ali poželjno):
	Stvoriti testne slučajeve gdje se vidi rad pojedine metode.
	
