from PIL import Image

#Resize picture so that width and height are the same and equal to dimension. If show_picture is true, display the picture
def resize(bytemap, dimension, show_picture=False):
    pass

#Get picture filename and turn into rgb bytemap. Must return 3 elements - red, green and blue channel.
def to_byte_map(picture):
    pass

#Gets RGB values for a picture and return grayscale. If show_picture is true, display the picture.
def grayscale(red_bytemap, green_bytemap, blue_bytemap, grayscale_levels, show_picture=False):
    pass
	
