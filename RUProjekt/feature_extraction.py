import numpy as np
from skimage.feature import greycomatrix, greycoprops


# Receives a GLC matrix and returns 4 features: contrast, homogeneity, energy, entropy
def calculate_haralick_features(glc_matrix):
    contrast = greycoprops(glc_matrix, 'contrast').mean(axis=1)
    energy = greycoprops(glc_matrix, 'energy').mean(axis=1)
    homogeneity = greycoprops(glc_matrix, 'homogeneity').mean(axis=1)
    return contrast, energy, homogeneity


# Receives a window (2d numpy matrix) and return a GLC matrix for given distance. The glc matrix is the average
# of matrices for 0, 45, 90, and 135 degrees.
def calculate_glc_matrix(window, distance):
    glc_matrix = greycomatrix(window, distances=[distance], angles=[0, 45, 90, 135], levels=256, symmetric=True,
                              normed=False)
    # glc_matrix_mean = np.sum(glcm[:, :, 0, :], axis=2)/4
    return glc_matrix


class Window:
    def __init__(self):
        pass


# For testing purposes only
if __name__ == '__main__':
    distance = 12
    test_image = np.array([i % 10 for i in range(96 * 96)]).reshape(96, 96)
    glcm = calculate_glc_matrix(test_image, distance)
    contrast, energy, homogeneity = calculate_haralick_features(glcm)
    print(contrast, energy, homogeneity)
