class Model:
    def __init__(self):
        pass

    # Exports learn_X and learn_y (numpy 2D matrices) and writes to file. 1 line per sample.
    def export_to_file(self, filename):
        pass

    # Imports samples from file, saves them to lists learn_X and learn_y and binds them to self.
    def import_from_file(self, filename):
        pass

    def learn(self):
        pass

    def predict(self):
        pass

    #
    def import_from_database(self, db_src):
        pass
