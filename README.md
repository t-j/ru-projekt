<h1>2. Detekcija i binarna klasifikacija slika uporabom lokalnih Haralickovih značajki teksture</h1>
<br>

<p>Oblikovati programski sustav za detekciju mnoštva ljudi i binarni klasifikator slika (razredi: mnoštvo ljudi, nema mnoštva ljudi). Sustav neka se temelji na pristupu sličnom onom koji je opisan u radu: <a href="https://bib.irb.hr/datoteka/639267.MLDM20130119.pdf">S. Ribaric and M. Lopar, Palmprint Recognition Based on Local Texture Features,  Machine Learning and Data Mining in Pattern Recognition, New York, 2013. pp. 119-127. </a></p>

Eksperimentalno odrediti rezoluciju slike, broj sivih razina, veličinu kliznog okna, korak posmicanja okna te izbor Haralickovih značajki koje dobrinose optimalnoj klasifikaciji.
Dodatno, na temelju odluke svakog od okana  odrediti granicu  područja mnoštva ljudi (za slučaj pozitivne klasifikacije slike).
Ocijeniti brzinu obrade i ukazati na mogućnost paralelizacije postupka u cilju primjene postupka za videosekvence.
Programsku implementaciju iscrpno ispitati na velikom broju slika mnoštva ljudi (makroskopska razina) i slika koje ne sadrže mnoštvo.
<hr>
Baze podataka:
<ul>
    <li> https://drive.google.com/file/d/134QOvaatwKdy0iIeNqA_p-xkAhkV4F8Y/view</li>
    <li> https://drive.google.com/file/d/17evzPh7gc1JBNvnW1ENXLy5Kr4Q_Nnla/view</li>
    <li> https://drive.google.com/file/d/1tdp0UCgxrqy1B6p8LkR-Iy0aIJ8l4fJW/view</li>
    <li> https://drive.google.com/file/d/18jFI789CoHTppQ7vmRSFEdnGaSQZ4YzO/view</li>
</ul>

<hr>
Dodatni izvor:
 <ul>
    <li><a href="http://www.csis.pace.edu/~ctappert/papers/proceedings/2016BTAS/Papers/FaceAnti-spoofingUsingHaralick.pdf">Face Anti-Spoofing using Haralick Features</a></li>
    <li><a href="https://link.springer.com/content/pdf/10.1007/s00138-017-0830-x.pdf">Detecting violent and abnormal crowd activity</a></li>
    <li><a href="http://haralick.org/journals/TexturalFeaturesHaralickShanmugamDinstein.pdf">Textural Features for Image Classification</a></li>
    <li><a href="https://d1wqtxts1xzle7.cloudfront.net/47052516/Automatic_estimation_of_crowd_density_us20160706-2281-846s3b.pdf?1467809728=&response-content-disposition=inline%3B+filename%3DAutomatic_estimation_of_crowd_density_us.pdf&Expires=1607166414&Signature=HN0jsgwrF3lEgaRBs1vb3hRZe21o0mdTkWYl1mt9LAhqUOhr3QaKbxiLC~-SDA3bUdAzMFdShOCXnN0dQq~yk3hhT7Fu-RCSeWQ3W160iGhC1HUlt6DzLrgq05R27deU29d83EJrBj0dNUfDwf25K~sTDpC7psRKUJkUpSXgyuEB9PltCGcROweQL36FTDhRJ3PgShfw2rQq3buCMA8pCuPLzNG5CBw49vJBegM-q3sj40c9mvmApZj~cyTk2axGJujKjBoVicKy3574Ooti2rVch84CWX5eYENRmwLyVJFZFX945AW1Z0tl17acLjA43MiwwV2UMrFDLaEYbZXMjg__&Key-Pair-Id=APKAJLOHF5GGSLRBV4ZA">Automatic estimation of crowd density</a></li>
    <li><a href="https://d1wqtxts1xzle7.cloudfront.net/40661695/Statistical_Structural_Texture_gray.pdf?1449363109=&response-content-disposition=inline%3B+filename%3DStatistical_and_structural_approaches_to.pdf&Expires=1607092294&Signature=FU9wnJupAGMZpcNab4kczo5Nbmx0ut5jw3wc1-QgNpEgN2CErWTgrIHj8AN7DfYTflLYFubH0Vjg48Yn0ED9G7wJEE0iIg8dPcovNvmQvup6VspKKX32w7qfWb1ZW9lZWhACsr8aNCqwvVmiMhNKhszX8IjkPCRGP4qyOF4-Pw8cAHkjH0HQbohmbx-XqKqBSheIFgNemgYYa8zTlfHGncqLAFjUqfPQE8jalM1UpNRam-MNt6qmKhoGNRoOFSd7NiEI~JnFY~7DoZKAYTXhnWT6~KHd1cb~mvo3-IQWnKvwBWMh5HsI8cfMt8fFCPLnOcIPO2RssPew90w6h-zoQw__&Key-Pair-Id=APKAJLOHF5GGSLRBV4ZA">Statistical and Structural Approaches to Texture (mozda nije bitan izvor)</a></li>
 </ul>
 <hr>
 <h3>Textural Features for Image Classification</h3>

 <ul>
    <li>Prva definicija i primjena Haralickovih značajki.</li>
    <li>Ukupno 14 značajki.</li>
    <li>Kada ljudi interpretiraju sliku 'koriste': spektralne, <b>teksturalne</b> i kontekstualne značajke.</li>
    <li>Haralickove značajke su <b>teksturalni</b> tip značajki</li>
    <li>Teksturalne značajke sadrže informaciju o prostornoj distribuciji varijacija nijansi u nekom pojasu. (dakle vise manje koliko su neke nijanse sive blizu jedna drugoj na dijelu slike)</li>
    <li>Zaključak je da teksturalne značajke mogu biti vrlo dobre diskriminacijske značajke.</li>
    <li>Glavna pretpostavka postupka je da je teksturalna informacija o nekom predmetu sadrzana u prosječnim relacijama koje nijanse sive imaju jedna prema drugoj. (Dakle, opet, koliko su neke nijanse sive blizu jedna drugoj na dijelu slike)</li>
    <li>U datoteci teorija su slikane najvaznije stvari za ovaj rad. </li>
 </ul>

<hr>
 <h3>Palmprint Recognition Based on Local Texture Features (Ribarićev rad)</h3>
 
 <ul>
    <li>Koriste se prve 3 Haralickove značajke - energija (Angular second momentum), kontrast i korelacija.</li>
    <li>Ako idete na SU, ovo je 1-KNN s n=3 (broj značajki).</li>
    <li>Gray tone spatial dependence matrix (Haralick) =  gray-level co-occurrence matrix (Ribarić) -> GLCM je normalizirana GTSDM (?)</li>
    <li>Postupak:
         <ol>
            <li>Imas sliku dimenzija (X,Y) piksela</li>
            <li>Imas "prozore" velicine d, d << X i d << Y</li>
            <li>Prozor stavis na gornji lijevi kut slike, radis iteraciju algoritma i pomaknes prozor za t prema desno ili dolje, t < d</li>
            <li>U jednoj iteraciji algoritma za neku preddefiniranu udaljenost izmedu piksela u prozoru -> izračuna GLCM za 0,45,90 i 135 stupnjeva</li>
            <li>Konačnu GLCM uzmes kao aritmetičku sredinu GLCM-a za 0,45,90 i 135 stupnjeva</li>
            <li>Računas Haralickove značajke iz konačne GLCM</li>
            <li>Pomoću značajki radis klasifikaciju</li>
         </ol>
    </li>
    <li>Velicina slike = (96, 96), broj nijansi sive... = 256, d = 12, t = d/2 = 6</li>
    <li>Dakle, za svaki prozor se za udaljenosti 1-6 izvlači 3 značajke...</li>
    <li>...pa je broj značajki za jednu sliku = ukupan broj prozora * broj značajki za jedan prozor =  225 * 18 = 4050 damn</li>
    <li>Funkcija sličnosti za 1-KNN je euklidska udaljenost (prvo se treba obaviti normalizacija)</li>
    <li>Poslije toga je sam treniranje modela td. je to vise manje to</li>
 </ul>

<hr>
<h3>Automatic estimation of crowd density</h3>
<ul>
   <li>Mjeri se koncentracija ljudi na nekom mjestu. Dozvoljene klase su - jako niska, niska, srednja, visoka, jako visoka.</li>
   <li>Rad sličan našem.</li>
   <li>Koriste se 4 Haralickove značajke - kontrast, homogenost, energija i entropija.</li>
   <li>Računa se GLDM za 0,45,90,135 stupnjeva i d=1</li>
   <li>E oni sada jos koriste i spektralne značajke koje ja brijem da nećemo koristit.</li>
   <li>O da koriste neuronski mrezu koju ne znam isto hoćemo li koristit.</li>
   <li>Mslm da najviše sšto mozemo izvuć iz ovoga je da je vjv dobro da koristimo iste H-značajke kao i oni.</li>
</ul>